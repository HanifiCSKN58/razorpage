﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RazorPage.Models
{
    public class Students
    {
        
        public int Id { get; set; }

        [Required]
        [MinLength(3), MaxLength(20)]
        [RegularExpression(@"^[a-zA-ZÖöıİşçÇğĞÜüŞ''-'\s]+$",
         ErrorMessage = "İsim alanına Rakam ve özel karakter girilemez.")]
        public string FirstName { get; set; }

        [Required]
        [MinLength(3), MaxLength(20)]
        [RegularExpression(@"^[a-zA-ZÖöıİşçÇğĞÜüŞ''-'\s]+$",
        ErrorMessage = "İsim alanına Rakam ve özel karakter girilemez.")]
        public string LastName { get; set; }

        [Required]
        [MinLength(3), MaxLength(20)]
        [RegularExpression(@"^[a-zA-ZÖöıİşçÇğĞÜüŞ0-9''-'\s]+$",
        ErrorMessage = "İsim alanına Rakam ve özel karakter girilemez.")]
        public string UserName { get; set; }


        [Required(ErrorMessage = "Şifre alanı boş olamaz.")]
        [MinLength(7), MaxLength(20)]
        [DataType(DataType.Password)]
        [RegularExpression(@"^[a-zA-Z0-9İıöÖüÜçÇğĞşŞ]{7,20}$", ErrorMessage = "En az 7, en fazla 20 karakter girilebilir Özel karakter girilemez!")]
        public string Password { get; set; }



        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Şifreler eşleşmiyor.")]
        [StringLength(20, ErrorMessage = "En az 7, en fazla 20 karakter girilebilir Özel karakter girilemez!", MinimumLength = 7)]
        public string ConfirmPassword { get; set; }


        [RegularExpression(@"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage = "Not a valid email")]
        public string Email { get; set; }
       
    }
}
