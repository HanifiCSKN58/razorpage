﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using RazorPage.Models;

namespace RazorPage
{
    public class StudentModel : PageModel
    {

        public List<Students> students { get; private set; }

        public void OnGet()
        {

            students = new List<Students> {
                new Students()
           {
               Id = 1,
               FirstName = "Fİrst Name 1",
               LastName = "Last Name 1",
               UserName = "User Name 1",
               Password = "Password 1",
               Email = "User1@gmail.com"
           },
                new Students()
                {
                    Id = 2,
                    FirstName = "Fİrst Name 2",
                    LastName = "Last Name 2",
                    UserName = "User Name 2",
                    Password = "Password 2",
                    Email = "User2@gmail.com"
                },
                new Students()
                {
                    Id = 3,
                    FirstName = "Fİrst Name 3",
                    LastName = "Last Name 3",
                    UserName = "User Name 3",
                    Password = "Password 3",
                    Email = "User3@gmail.com"
                }
                };
        }
        [BindProperty]
        public Students Students { get; set; }
        public IActionResult OnPostAaa()
        {
            students = new List<Students> {
                new Students()
           {
               Id = 1,
               FirstName = "Fİrst Name 1",
               LastName = "Last Name 1",
               UserName = "User Name 1",
               Password = "Password 1",
               Email = "User1@gmail.com"
           },
                new Students()
                {
                    Id = 2,
                    FirstName = "Fİrst Name 2",
                    LastName = "Last Name 2",
                    UserName = "User Name 2",
                    Password = "Password 2",
                    Email = "User2@gmail.com"
                },
                new Students()
                {
                    Id = 3,
                    FirstName = "Fİrst Name 3",
                    LastName = "Last Name 3",
                    UserName = "User Name 3",
                    Password = "Password 3",
                    Email = "User3@gmail.com"
                },
                 new Students()
                {
                    Id = 4,
                    FirstName =Students.FirstName,
                    LastName = Students.LastName,
                    UserName = Students.UserName,
                    Password =Students.Password,
                    Email = Students.Email
                }
                };
            return RedirectToPage("/Student/Student");
        }



    }
}