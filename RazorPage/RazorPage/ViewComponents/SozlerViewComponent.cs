﻿using Microsoft.AspNetCore.Mvc;
using RazorPage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RazorPage.ViewComponents
{
    [ViewComponent]
    public class SozlerViewComponent:ViewComponent
    {

        public List<Sozler> sozlers { get; private set; }

        public IViewComponentResult Invoke()
        {

            sozlers = new List<Sozler>()
        {
            new Sozler(){Id= 1 ,Soz = "Hırs başarısızlığın son sığınağıdır. "},
            new Sozler(){Id= 2 ,Soz = "Zihin fukara olunca akıl ukala olurmuş. "},
            new Sozler(){Id= 3 ,Soz = "Seni hayallerine ulaştıracak en önemli şey, cesaretindir. "},
            new Sozler(){Id= 4 ,Soz = "Kod, espiri gibidir. Açıklamak zorundaysanız kötüdür.   Cory House"},
            new Sozler(){Id= 5 ,Soz = "Bedava peynir sadece fare kapanında olur.  Ahmet Şerif İzgören"},
            new Sozler(){Id= 6 ,Soz = "Kod yalan söylemez, yorumlar bazen söyler.    Ron Jeffries "},
        };
            Random random = new Random();
            int num = random.Next(1, sozlers.Count);
       
             var  deg = (from sozler in sozlers where sozler.Id == num select sozler).FirstOrDefault();
       
            return View(deg); 


        }
    }
}