﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ViewComponentDersi.Models;

namespace ViewComponentDersi.ViewComponents
{
    [ViewComponent]
    public class Duyuru1ViewComponent:ViewComponent
    {
        public List<DuyurularModel> duyurulars { get; private set; }

        public IViewComponentResult Invoke(int id)
        {

            duyurulars = new List<DuyurularModel>()
        {
            new DuyurularModel(){Id= 1 ,Duyuru = "Hırs başarısızlığın son sığınağıdır. "},
            new DuyurularModel(){Id= 2 ,Duyuru = "Zihin fukara olunca akıl ukala olurmuş. "},
            new DuyurularModel(){Id= 3 ,Duyuru = "Seni hayallerine ulaştıracak en önemli şey, cesaretindir. "},
            new DuyurularModel(){Id= 4 ,Duyuru = "Kod, espiri gibidir. Açıklamak zorundaysanız kötüdür.   Cory House"},
            new DuyurularModel(){Id= 5 ,Duyuru = "Bedava peynir sadece fare kapanında olur.    Ahmet Şerif İzgören"},
            new DuyurularModel(){Id= 6 ,Duyuru = "Kod yalan söylemez, yorumlar bazen söyler.    Ron Jeffries "},
        };
          var  duyuru= (from sozler in duyurulars where sozler.Id == id select sozler).FirstOrDefault();

            return View(duyuru);


        }
    }
}
